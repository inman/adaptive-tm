$(document).ready(function(){
	
  
	/*slider-on-header*/
  
	$('.bxslider').bxSlider({
		mode: 'fade',
		controls: false,
		auto: true,
		pause: 5000
	});
	
	/*href*=#*/
	
	$(document).ready(function(){
	   $('a[href*=#]').bind("click", function(e){
	      var anchor = $(this);
	      $('html, body').stop().animate({
	         scrollTop: $(anchor.attr('href')).offset().top
	      }, 1000);
	      e.preventDefault();
	   });
	   return false;
	});
	
	/*menu*/
	
	$(function() {
        var pull 		= $('#pull');
            menu 		= $('nav ul');
            menuHeight	= menu.height();

        $(pull).on('click', function(e) {
            e.preventDefault();
            menu.slideToggle();
        });

        $(window).resize(function(){
    		var w = $(window).width();
    		if(w > 320 && menu.is(':hidden')) {
    			menu.removeAttr('style');
    		}
	});
    });
    
	
  	
	/*lightbox-portfolio*/
	
	$(function() {
	    $('.lightbox a').lightBox();
	});
	
	function hide_show()
	{
	    if ($('.lightbox li:hidden').length==0){
	        $('.load_more span').hide();
	    }
	    else{
	        $('.load_more span').show();
	    }
	}

	$(document).ready(function () {
	    size_li = $(".lightbox li").size();
	    x=6;
	    $('.lightbox li:lt('+x+')').show();
	    $('.load_more span').click(function () {
	        x= (x+3 <= size_li) ? x+3 : size_li;
	        $('.lightbox li:lt('+x+')').show();
	        hide_show();
	    });
	});
	
	/*carousel*/
	
    $(document).ready(function() {
     
      $("#owl-demo").owlCarousel({
     
          navigation : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination: true,
          autoPlay: true
     
      });
     
    });
	
	
	/*open-hide*/
	
	$(document).ready(function(){
		$('.content').hide();
		$('.hide').click(function(){
			$(this).toggleClass('open').next().toggle();
			$('.content').toggleClass('connew');
			});
		
	});
	
	/*circle-diagram*/
	
	$(function() {

		$('#diagram-id-1').circleDiagram();
		$('#diagram-id-2').circleDiagram();
		$('#diagram-id-3').circleDiagram();
		$('#diagram-id-4').circleDiagram();

	});
	
	
	/*google-map*/
	
	// When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'Snazzy!'
                });
            }
	
});